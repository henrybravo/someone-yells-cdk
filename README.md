# someone-yells-cdk

Example of deploying an containerized [webapp]( https://gitlab.com/henrybravo/someone-yells) to [AWS Fargate](https://aws.amazon.com/fargate) with the AWS Cloud Development Kit - [AWS CDK](https://github.com/awslabs/aws-cdk)

## aws-cdk
The AWS Cloud Development Kit [(AWS CDK)](https://docs.aws.amazon.com/cdk/latest/guide/home.html) is an open-source software development framework to define cloud infrastructure in code and provision it through AWS CloudFormation [more...](https://docs.aws.amazon.com/cdk/latest/guide/home.html)

## getting started

- You must install [Node.js (>= 8.11.x)](https://nodejs.org/en/download) to use the command-line toolkit and language bindings.
- Specify your credentials and region with the AWS CLI. You must specify both your credentials and a region to use the toolkit.

## Install or update the AWS CDK Toolkit from npm (requires Node.js ≥ 8.11.x):

```bash
$ npm i -g aws-cdk
```

For a detailed walkthrough, see [Getting Started](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html) or jump directly to the [AWS CDK Reference Documentation](https://docs.aws.amazon.com/cdk/api/latest/)

## initialize cdk in an empty directory

```bash
$ mkdir someone-yells-app && cd someone-yells-app

$ cdk init --language typescript

$ npm run build && cdk synth && npm install @aws-cdk/aws-ec2 @aws-cdk/aws-ecs @aws-cdk/aws-ecs-patterns
```

## create the service and construct the stack

add the following into the generated `lib/someone-yells-app-stack.ts`

```ts
import * as cdk from '@aws-cdk/core';

import ec2 = require('@aws-cdk/aws-ec2');
import ecs = require('@aws-cdk/aws-ecs');
import ecs_patterns = require("@aws-cdk/aws-ecs-patterns");

export class SomeoneYellsAppStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
	const vpc = new ec2.Vpc(this, "MyVpc", {
	  maxAzs: 3 // Default is all AZs in region
	});

	const cluster = new ecs.Cluster(this, "MyCluster", {
	  vpc: vpc
	});

	// Create a load-balanced Fargate service and make it public
	new ecs_patterns.ApplicationLoadBalancedFargateService(this, "MyFargateService", {
	cluster: cluster, // Required
	cpu: 256, // Default is 256
	desiredCount: 6, // Default is 1
	taskImageOptions: { 
			image: ecs.ContainerImage.fromRegistry("henrybravo/hostyells"),
			containerPort: 3000
		},
	memoryLimitMiB: 512, // Default is 512
	publicLoadBalancer: true // Default is false
	});
  }
}
```

This deploys a Fargate container terminated by a publicLoadbalancer listening on port 80. 

# build and deploy the stack

```bash
npm run build && cdk synth
cdk deploy
```

After about 3 minutes the deployment should complete and the LB endpoint will be visible in the Outputs line from the previous command, i.e.

```bash
49/49|2:43:14PM|CREATE_COMPLETE|AWS::CloudFormation::Stack|SomeoneYellsAppStack

 ✅  SomeoneYellsAppStack

Outputs:
SomeoneYellsAppStack.MyFargateServiceLoadBalancer = ***.us-east-1.elb.amazonaws.com
```

If for some reason the deployment fails i.e. "The maximum number of internet gateways has been reached" you can delete the rollbacked stack with the aws-cli:

```bash
aws cloudformation delete-stack --stack-name SomeoneYellsAppStack
```

## Clean up

```bash
$ cdk destroy SomeoneYellsAppStack
```